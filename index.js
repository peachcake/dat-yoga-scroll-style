const choo = require('choo')
const devtools = require('choo-devtools')

// Views
const cover = require('./javascripts/views/cover')
const main = require('./javascripts/views/main')

// Stores
const core = require('./javascripts/stores/core')
const kriya = require('./javascripts/stores/kriya')

var app = choo()
app.use(devtools())

// initialize the stores
app.use(core)
app.use(kriya)

// Setup the routes
app.route('/', cover)
app.route('/steps', main)

// mount choo onto homepage
app.mount('body')
