# Introduction

This is a dat zine following the Dat Yoga Zine Scroll style!  If you're reading the source, then you likely copied the zine cos you wanted to make your zine, or are about to do this.  Hell yeah awesome!

  This zine style is meant to take a yoga sequence you give as a text file and render it into a nice zine with timers.  This works especially well for the [kundalini](https://www.libraryofteachings.com/) tradition of yoga.  You can have gifs or images for each pose if you'd like, or just leave it blank.

  Once you copy this site, you wll have full control over each part of the zine--you can change it up however you'd like, you can even change up this readme if you want!  The code is designed to help with this, giving you different degrees of customization depending on how 'into the code' you wanna get. They can be orgnaized as:

  **_CHANGE THE TEXT AND IMAGES_**
  **_CHANGE THE AESTHETICS_**
  **_CHANGE THE CODE_**

  How to make each change! 

  1.) You can change just the yoga sequence, by navigating to [kriya/kriya.txt](kriya/kriya.txt).  You can edit it using beaker's built-in editor, and it doesn't require any coding.  There is another readme in that folder with more details.

  2.) You can adjust the overall style of this zine (but not its structure) by navigating to the [aesthetic](aesthetic/README.md) folder.  Here you will find two CSS files.  One requires no coding knowledge (except for how to grab the hex code for a color and paste it in your file).  The other file takes simple CSS, and so you can code it as much as you are able/willing at this time in yr life!  There are more details in the Aesthetic folder.

  3.) If you want to change anything deeper than aesthetic, you can do so but it will require a bit of setup first (making sure you have [node](https://nodejs.org) and [npm](https://npmjs.com) installed).  This zine is written using the framework [choo](https://choo.io).  I also wrote all of this as an elaborate journal entry of sorts that you can read (and code within) in the git repo of this zine style, found here: https://gitlab.com/dat-zines/dat-yoga-scroll-style/

To get the most out of that file, you will want to install the text-editor/mystic-exprience [Spacemacs](http://spacemacs.org/).  **_If you wanna work on the code of this, but not sure what node and npm and such means, please reach out to me at webmaster@coolguy.website.  I would be happy to help you get started!_**

  There are additional README's in each folder to give you better context.  And again for the busy you: **If you just want to change the yoga sequence, check out the kriya folder.**

# Thank You's

My biggest thanks to Angelica, and her awesome yoga zines: [Winter Workshop](https://winteryoga.hashbase.io/) and [5 Minute computer Calm](https://computer-calm.hashbase.io/).  These were the inspiration for this here template, and are just delightful.

Thanks to the creators and maintainers of [choo](choo.io) and [smarkt](https://github.com/jondashkyle/smarkt) for the easy to use code these zines be built around.

Thanks to the [Beaker Community](https://p2p.taravancil.com/) and the [Scuttlebutt Community](https://scuttlebutt.nz) for starting up the Chorus in which all these zines reside.

And thanks to the golden chain of teachers that led to the gifts contained in this zine!

# Music and Life

I listened to a lot of [Haiku Salut](https://haikusalut.bandcamp.com/), [Car Seat Headrest](https://carseatheadrest.bandcamp.com/), and [Shawn Wasabi](https://www.youtube.com/watch?v=NzGGL0VpKUI) during the making of this zine style.  It was made in Miramar, Wellington while house sitting and going to national sheep parks, and in Hataitai, Wellington, on the living room couch watching two cats square up to, but never actually, fight.
