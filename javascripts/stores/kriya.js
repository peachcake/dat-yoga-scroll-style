const smarkt = require('smarkt')

var archive = new DatArchive(window.location)

module.exports = (state, emitter) => {
    state.kriya = {}
    emitter.on('DOMContentLoaded', () => {
        archive.readFile('kriya/kriya.txt').then(text => {
            state.kriya = smarkt.parse(text)
            emitter.emit('render')
        })
    })
}
