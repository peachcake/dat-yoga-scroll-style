const html = require('nanohtml')

module.exports  = (state, emit, step) => {
  return html`
           <li>
             <h2>${step.name}</h2>
             <p>${step.description}</p>
             <h3>${step.length}</h3>
           </li>
`
}
