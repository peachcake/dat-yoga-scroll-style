const html = require('nanohtml')
const _ = require('lodash')

module.exports = (state, emit) => {
    if (state.kriya) {
     name = _.startCase(state.kriya.name)
        return html`
    <section id='name'>
     <h1 class='main-title'>${name}</h1>
     <h2>${state.kriya.description}</h2>
    </section>
  `
    }
}
