const html = require('nanohtml')

const timedStep = require('./timedStep')
const repStep = require('./repStep')

module.exports = (state, emit) => {
  if (state.kriya.steps) {
    return html`
        <ul id='stepList'>
          ${listSteps(state.kriya.steps)}
        </ul>
        `
  }

  function listSteps (steps) {
    var stepNumbers =  Object.keys(steps)
    return stepNumbers.map(number => {
      var step = steps[number]
      step.number = number
      return correctStep(step)
    })
  }

  function correctStep (step) {
    return isTimed(step) ? timedStep(state, emit, step) : repStep(state, emit, step)
  }

  function isTimed (step) {
    if (!step.length) {
      console.log(`${step.name} has no length property.  It should be added.`)
      return false
    } else {
      var length = step.length.toLowerCase()
      return (length.includes('minute') || length.includes('second'))
    }
  }
}
