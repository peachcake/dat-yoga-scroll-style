const Nanocomponent = require('nanocomponent')
const nanostate = require('nanostate')
const html = require('nanohtml')
const _ = require('lodash')

class Timer extends Nanocomponent {
  constructor (name, state, emit) {
    super(name, state, emit)
    this.state = {
      remaining: 0,
      display: '',
      timeTracker: null
    }
    this.triggerCountdown = this.triggerCountdown.bind(this)
    this.pauseCountdown = this.pauseCountdown.bind(this)
    this.countdown = this.countdown.bind(this)
    this.playSwell = this.playSwell.bind(this)

    this.machine = nanostate('off', {
      off: { click: 'counting'},
      counting: { click: 'paused', finished: 'done'},
      paused: { click: 'counting'},
      done: { click: 'off'}
    })
    this.machine.on('counting', () => this.triggerCountdown(this.state))
    this.machine.on('paused', () => this.pauseCountdown())
    this.machine.on('done', () => {
      this.pauseCountdown()
      this.playSwell()
      this.state.display = 'Done!'
    })
  }

  createElement (length) {

    var buttonText = {
      off: 'Start Timer',
      counting: 'Pause Timer',
      paused: 'Resume Timer',
      done: 'Done baby!'
    }[this.machine.state]

    var buttonClass = {
      off: 'timer off',
      counting: 'timer counting',
      paused: 'timer paused',
      done: 'timer done'
    }[this.machine.state]


    if (this.machine.state === 'off' && this.state.remaining === 0) {
      this.state.remaining = toSeconds(length)
    }

    if (this.machine.state === 'counting' && this.state.remaining === 0) {
      this.machine.emit('finished')
    }

    if (this.machine.state === 'done') {
      buttonText = 'Reset?'
      this.state.display = 'Done!'
    } else {
      this.state.display = toDisplay(this.state.remaining)
    }

    return html`
                   <section class='timerBox'>
                     <p>${this.state.display}</p>
                     <button class=${buttonClass}
                             onclick=${()=> this.click()}
                      >
                       ${buttonText}
                     </button>
                   </section>
                 `
  }
  update (length) {
    return false
  }
  click () {
    this.machine.emit('click')
    this.rerender()
  }

  triggerCountdown () {
    this.state.timeTracker = setInterval(this.countdown, 1000)
  }
  pauseCountdown () {
    clearInterval(this.state.timeTracker)
  }
  countdown () {
    this.state.remaining -= 1
    this.rerender()
  }
  playSwell () {
    var swell = new Audio('./kriya/assets/chimes/down-into-body.mp3')
    swell.play();
  }
}

function toSeconds (length) {
  length = length.toLowerCase()
  var seconds = 0
  if (length.includes('minute') && !length.includes('second')) {
    length = _.words(length, /[0-9]+/)
    seconds = parseInt(length) * 60
  } else if (length.includes('minute') && length.includes('second')){
    var times = _.words(length, /[0-9]+/g)
    seconds = (parseInt(times[0])*60) + parseInt(times[1])
  } else {
    var justTime = _.words(length, /[0-9]+/)[0]
    seconds = parseInt(justTime)
  }
  return seconds
}

function toDisplay (length) {
  var minutes = Math.floor(length / 60)
  var seconds = length - minutes * 60
  return contextualDisplay(minutes, seconds)
}

function contextualDisplay (minutes, seconds) {
  var minute = `${minutes} minutes`
  var and = ` and `
  var second = `${seconds} seconds`
  if (minutes === 1) minute = `${minutes} minute`
  if (minutes === 0) minute = ''
  if (seconds === 1) second = `${seconds} second`
  if (seconds === 0) second = ''
  if (seconds === 0 || minutes === 0) and = ''
  return minute + and + second
}

module.exports = Timer
