const html = require('nanohtml')
const Timer = require('./Timer')

module.exports  = (state, emit, step) => {
  var timer = new Timer()
  return html`
           <li class='step'>
             <h2>${step.number}. ${step.name}</h2>
             <p>${step.description}</p>
             ${checkForAssets(step)}
             ${timer.render(step.length)}
           </li>
`
  function checkForAssets (step) {
    if (step.assets.length > 0) {
      return renderAssets(step)
    }
  }

  function renderAssets (step) {
    return step.assets.map(asset => {
      return html`
        <div class='asset'>
          <img src=${asset} alt='image showing ${step.name}' />
        </div>
        `
    })
  }
}
