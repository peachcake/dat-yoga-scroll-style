const html = require('nanohtml')
const name = require('./name')
const steps = require('./steps')

module.exports = (state, emit) => {
  return html`
    <body>
      <header>
      ${name(state, emit)}
      </header>
    <main id='kriya'>
      ${steps(state, emit)}
    </main>
    </body>
`
}
