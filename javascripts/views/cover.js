const html = require('choo/html')

module.exports = (state, emit) => {
  return html`
<body>
<h1>${state.kriya.name}</h1>
<p>${state.kriya.description}</p>
<a href='#steps'>Enter</a>
</body>
`
}
